def writeToFile(filename, type_access, data):
    '''write data to file

    @param:

    filename -- The path to the data filename
    type_access -- type of accessing file
    '''
    file = open(filename, type_access)
    file.write(data + '\n')
    file.close()


def loadAtribut(filename):
    '''Load the atrbutes of data

    @param:

    filename -- The path to the data filename
    
    return:
    1D list of the atributes
    '''
    atribut = []
    with open(filename) as f:
        for line in f:
            line = line.split(';')
            for atr in line:
                atribut.append(atr.strip())
    f.closed
    return atribut


def convertData(isFloat, data):
    if isFloat:
        return float(data)
    else:
        return int(data)


def loadData(filename, isFloat):
    '''Load the data

    @param:

    filename -- The path to the data filename

    return:
    2D list of the data (class include)
    '''
    data = []
    with open(filename) as f:
        for line in f:
            line = line.split(';')
            atribut = []
            for atr in line:
                # print(atr)
                atr = atr.split('ï»¿')
                if (len(atr) > 1):
                    atribut.append(convertData(isFloat, atr[1].strip()))
                else:
                    atribut.append(convertData(isFloat, atr[0].strip()))
            data.append(atribut)
    f.closed
    return data
