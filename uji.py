import math
from process import processForward, normalizeInput, generateLayer
from loader import writeToFile, loadAtribut, loadData

minVal = [0.0, 0.0, 0.0, 0.0]
maxVal = [255.0, 255.0, 255.0, 100.0]

bias_Hidden = [3.7495019369330795,
 -3.656327540126608,
 -9.984680452117006,
 -11.152360414417029]

weight_Hidden =[[-9.53752943615178,
  3.5824102760236065,
  -1.0525836867996385,
  2.4722760114555387],
 [7.196365805742117, -1.0641718207814663, -1.52601546240707, 6.447708677171792],
 [30.91914126872395, -8.233612348898147, 1.203229686445866, 8.966403595091826],
 [14.745179221824337, 8.684840792953562, -9.407265646062209, 27.76462245804956]]

bias_Output =[-8.335209788224898, -2.859117305837435, 5.227893654004517]
weight_Output = [[-0.8242659526354096,
  4.379764526620683,
  -0.9353254386235187,
  20.635342274207932],
 [-7.305441178679144,
  0.4969536488032304,
  15.488341034347345,
  -18.743997373643523],
 [2.9723409314589313,
  -6.3519774898475845,
  -9.881028570934102,
  -12.6057285686095]]


def activation(y):
    new_y = []
    for i in range(len(y)):
        temp = round(y[i])
        # if y[i] * 10 > 1:
        #     temp = 1
        new_y.append(temp)
    return new_y


def output_activation(y):
    out = y.index(max(y))
    hasil = ""
    if out == 0:
        hasil = "kelas 1 \t" + str(out)
    elif out == 1:
        hasil = "kelas 2 \t" + str(out)
    elif out == 2:
        hasil = "kelas 3 \t" + str(out)
    else:
        hasil = "unknown \t" + str(out) + str(y)
    return hasil


atribut = loadAtribut('./DATA/atributl.txt')
dataTest = loadData('./DATA/datal.csv', 1)
target = loadData('./DATA/targetl.csv', 0)
LAYER_HIDDEN = generateLayer(len(atribut))
LAYER_OUTPUT = generateLayer(len(target[0]))
for i in range(len(dataTest)):
    z_in, z, y_in, y = processForward(
        normalizeInput(dataTest[i], minVal, maxVal), bias_Hidden, bias_Output,
        weight_Hidden, weight_Output)
    # z_in, z, y_in, y = processForward(
    #     dataTest[i], bias_Hidden, bias_Output,
    #     weight_Hidden, weight_Output)
    writeToFile('./DATA/hasilL.txt', 'a', output_activation(y))
    print(y)