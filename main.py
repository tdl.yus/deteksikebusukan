import pprint
from loader import *
from process import *

# load data
atribut = loadAtribut('./DATA/atributl.txt')
dataTest = loadData('./DATA/datal.csv', 1)
target = loadData('./DATA/targetl.csv', 0)
# learning rate
# print(target)
alpha = 0.2
# maximum epoch
maxEpoch = 10000
dataTest, minVal, maxVal = normalizeData(dataTest)

layer_Hidden = len(atribut)  #ganti terserah banyak nodenya
layer_Output = len(target[0])  #jangan diganti
bias_Hidden = generateBias(layer_Hidden, 1)  #jangan diganti
bias_Output = generateBias(layer_Output, 1)  #jangan diganti
weight_Hidden = generateRandomWeight(layer_Hidden, len(atribut),
                                     (0, 1))  #jangan diganti
weight_Output = generateRandomWeight(layer_Output, layer_Hidden,
                                     (0, 1))  #jangan diganti

bias_Hidden, weight_Hidden, bias_Output, weight_Output = process(
    dataTest, target, bias_Hidden, bias_Output, weight_Hidden, weight_Output,
    maxEpoch, alpha)

print()
pprint.pprint("Min Value")
pprint.pprint(minVal)
pprint.pprint("Max Value")
pprint.pprint(maxVal)
pprint.pprint("bias Hidden")
pprint.pprint(bias_Hidden)
pprint.pprint("weight hidden")
pprint.pprint(weight_Hidden)
pprint.pprint("bias output")
pprint.pprint(bias_Output)
pprint.pprint("weight_output")
pprint.pprint(weight_Output)