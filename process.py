import random
import math
import sys


def progress(count, total, status=''):
    '''
    Display a progress bar on console
    '''
    bar_len = 40
    filled_len = int(round(bar_len * count / float(total)))
    percents = round(100.0 * count / float(total), 1)
    bar = '#' * filled_len + '-' * (bar_len - filled_len)
    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
    sys.stdout.flush()


def normalizeInput(inputData, minVal, maxVal):
    '''
    Compute normalization input data with min value and max value from normalization data
    @param
    inputData -- 1D list value of input data
    minVal -- 1D list of value of min value attribut
    maxVal -- 1D list of value of max value attribut

    return:
    normalizedInput
    '''
    for idx_atr in range(len(inputData)):
        inputData[idx_atr] = (inputData[idx_atr] - minVal[idx_atr]) / (
            maxVal[idx_atr] - minVal[idx_atr])
    return inputData


def normalizeData(data):
    '''
    Compute normalization data
    @param
    data -- 2D list of data

    return:
    data, min_val_atr, max_val_atr
    '''
    minVal = [0.0,0.0,0.0,0.0]
    maxVal = [255.0,255.0,255.0,100.0]

    # # generate first minVal and maxVal
    # for idxAtr in range(len(data[0])):
    #     minVal.append(9999)
    #     maxVal.append(-9999)

    # find min value and max value
    # for data_per_line in data:
    #     for idx_atr in range(len(data_per_line)):
    #         if (minVal[idx_atr] > data_per_line[idx_atr]):
    #             minVal[idx_atr] = data_per_line[idx_atr]
    #         if (maxVal[idx_atr] < data_per_line[idx_atr]):
    #             maxVal[idx_atr] = data_per_line[idx_atr]

    # normalization min max 0 - 1
    for idx_data in range(len(data)):
        for idx_atr in range(len(data[idx_data])):
            data[idx_data][
                idx_atr] = (data[idx_data][idx_atr] - minVal[idx_atr]) / (
                    maxVal[idx_atr] - minVal[idx_atr])

    return data, minVal, maxVal


def generateLayer(number):
    '''Generate the number of layer
    @param:
    number -- number of layer want to build

    return:
    1D list of layer
    '''
    temp = []
    for i in range(number):
        temp.append(0)
    return temp


def generateRandomWeight(number, length, interval):
    '''Generate random value for initialize
    @param:
    number -- number of next layer
    length -- length of the list result
    interval -- interval of random value (0-interval)

    return:
    2D list of the random value from number of layer to length of atributes
    '''
    tempLayer = []
    for i in range(number):
        tempAtribute = []
        for j in range(length):
            tempAtribute.append(random.uniform(interval[0], interval[1]))
        tempLayer.append(tempAtribute)
    return tempLayer


def generateBiasRandom(length):
    '''Generate random value for initialize bias
    @param:
    length -- length of the list result

    return:
    1D list of the random
    '''
    bias = []
    for i in range(length):
        bias.append(random.random())
    return bias


def generateBias(length, val):
    '''Generate bias
    @param:
    length -- length of the list result
    val -- value to set into bias

    return:
    1D list of the bias
    '''
    bias = []
    for i in range(length):
        bias.append(val)
    return bias


def activationFunctionBinary(d):
    hasil = 1.0 / (1.0 + math.exp(-d))
    return hasil


def activationFunctionBinaryDerivative(d):
    hasil = activationFunctionBinary(d) * (1.0 - activationFunctionBinary(d))
    return hasil


'''
forward propagation
'''


def process_sum(bias, weight, inputData):
    '''
    Do process summation of bias, weight, and input
    output_in = bias + sigma(weight*input)
    output = f(out_in)
    @param:
    bias -- list of bias value
    weight -- list of weight value
    input -- list of input value

    return:
    1D list of output_in, 1D list of output
    '''
    out_in = []
    out = []
    for i in range(len(bias)):
        temp_in = 0
        for j in range(len(weight[i])):
            temp_in += inputData[j] * weight[i][j]
        temp_in += bias[i]
        out_in.append(temp_in)
        temp = activationFunctionBinary(temp_in)
        out.append(temp)
    return out_in, out


def processForward(dataTestPerLine, bias_Hidden, bias_Output, weight_Hidden,
                   weight_Output):
    '''
    Do process forward
    @param:
    dataTestPerLine -- 1D list of data set
    bias_Hidden -- list of hidden bias
    bias_Output -- list of output bias
    weight_Hidden -- 2D list of hidden weight
    weight_Output -- 2D list of output weight

    return:
    z_in, z, y_in, y
    '''
    z_in, z = process_sum(bias_Hidden, weight_Hidden, dataTestPerLine)
    y_in, y = process_sum(bias_Output, weight_Output, z)
    return z_in, z, y_in, y


'''
Backward propagation
'''


def updateWeight(weight, delta_weight):
    '''
    Compute the update of the weight

    @param
    weight -- 2D list of weight
    delta_weight -- 2D list of delta weight

    return:
    new_weight
    '''
    new_weight = []
    for i in range(len(weight)):
        temp_weight = []
        for j in range(len(weight[i])):
            temp = weight[i][j] + delta_weight[i][j]
            temp_weight.append(temp)
        new_weight.append(temp_weight)
    return new_weight


def updateBias(bias, delta_bias):
    '''
    Compute the update of the bias

    @param
    bias -- 1D list of Bias value
    delta_bias -- 1D list of the delta value of bias

    return:
    new_bias
    '''

    new_bias = []
    for i in range(len(bias)):
        temp = bias[i] + delta_bias[i]
        new_bias.append(temp)
    return new_bias


def backpropagation_Output(target, y, y_in, z, alpha):
    '''
    Compute the error information term for output

    @param
    target -- list of the data target
    y -- list of the output after activation
    y_in -- list of the output before activation
    z -- list of the hidden output before activation
    alpha -- learning rate (float)

    return:
    delta_weight_Output, delta_bias_Output, errorInfo_output
    '''
    errorInfo_Output = []
    delta_weight_Output = []
    delta_bias_Output = []
    for idx_output in range(len(y)):
        # print('error => (' + str(target[idx_output]) + ' - ' +
        #       str(y[idx_output]) + ') * ' +
        #       str(activationFunctionBinaryDerivative(y_in[idx_output])))
        errorInfo = (target[idx_output] - y[idx_output]
                     ) * activationFunctionBinaryDerivative(y_in[idx_output])
        # errorInfo = (target[idx_output] - y[idx_output]
        #              ) * y[idx_output] * (1-y[idx_output])
        errorInfo_Output.append(errorInfo)
        temp_z = []
        for idx_z in range(len(z)):
            temp = alpha * errorInfo * z[idx_z]
            temp_z.append(temp)
        delta_weight_Output.append(temp_z)
        delta_bias_Output.append(alpha * errorInfo)

    return delta_weight_Output, delta_bias_Output, errorInfo_Output


def backpropagation_Hidden(errorInfo_Output, weight_Output, z_in, dataPerLine,
                           alpha):
    '''
    Compute the information term for hidden layer
    @param
    errorInfo_Output -- list error information output from output layer
    weight_Output -- 2D list of weight_Output
    z_in -- list output from hidden layer before activation
    dataPerLine -- list of atribut of data
    alpha -- learning rate value

    return:
    delta_weight_Hidden, delta_bias_Hidden
    '''
    delta_weight_Hidden = []
    delta_bias_Hidden = []
    for idx_h_l in range(len(z_in)):
        errorInfo_Hidden = 0
        for idx_e_o in range(len(errorInfo_Output)):
            errorInfo_Hidden += errorInfo_Output[idx_e_o] * weight_Output[idx_e_o][idx_h_l]
        errorInfo_Hidden = errorInfo_Hidden * activationFunctionBinaryDerivative(
            z_in[idx_h_l])
        temp_delta_weight = []
        for idx_atr in range(len(dataPerLine)):
            temp = alpha * errorInfo_Hidden * dataPerLine[idx_atr]
            temp_delta_weight.append(temp)
        delta_weight_Hidden.append(temp_delta_weight)
        temp_bias = alpha * errorInfo_Hidden
        delta_bias_Hidden.append(temp_bias)

    return delta_weight_Hidden, delta_bias_Hidden


def processBackward(dataTestPerLine, target, bias_Hidden, bias_Output,
                    weight_Hidden, weight_Output, z_in, z, y_in, y, alpha):
    '''
    Do process backward
    @param
    dataTest -- 2D list of data set
    target -- list of the data target
    bias_Hidden -- list of hidden bias
    bias_Output -- list of output bias
    weight_Hidden -- 2D list of hidden weight
    weight_Output -- 2D list of output weight
    z_in -- list z_in value
    z -- list z value
    y_in -- list y_in value
    y -- list y value
    alpha -- learning rate (float)

    return:
    bias_hidden , weight_hidden, bias_output, weight_output
    '''
    delta_weight_Output, delta_bias_Output, errorInfo_Output = backpropagation_Output(
        target, y, y_in, z, alpha)
    delta_weight_Hidden, delta_bias_Hidden = backpropagation_Hidden(
        errorInfo_Output, weight_Output, z_in, dataTestPerLine, alpha)
    bias_Output = updateBias(bias_Output, delta_bias_Output)
    weight_Output = updateWeight(weight_Output, delta_weight_Output)
    bias_Hidden = updateBias(bias_Hidden, delta_bias_Hidden)
    weight_Hidden = updateWeight(weight_Hidden, delta_weight_Hidden)

    return bias_Hidden, weight_Hidden, bias_Output, weight_Output


def process(dataTest, target, bias_Hidden, bias_Output, weight_Hidden,
            weight_Output, maxEpoch, alpha):
    '''
    Do process propagation
    @param:
    dataTest -- 2D list of data set
    target -- list of the data target
    bias_Hidden -- list of hidden bias
    bias_Output -- list of output bias
    weight_Hidden -- 2D list of hidden weight
    weight_Output -- 2D list of output weight
    maxEpoch -- max iterate of epoch
    alpha -- learning rate (float)

    return:
    bias_Hidden, weight_Hidden, bias_Output, weight_Output
    '''
    i = 0
    z_in, z, y_in, y = 0, 0, 0, 0
    while (i < maxEpoch):
        i += 1
        progress(i, maxEpoch, status='Train system')
        for idx_data in range(len(dataTest)):
            z_in, z, y_in, y = processForward(dataTest[idx_data], bias_Hidden,
                                              bias_Output, weight_Hidden,
                                              weight_Output)
            bias_Hidden, weight_Hidden, bias_Output, weight_Output = processBackward(
                dataTest[idx_data], target[idx_data], bias_Hidden, bias_Output,
                weight_Hidden, weight_Output, z_in, z, y_in, y, alpha)

    return bias_Hidden, weight_Hidden, bias_Output, weight_Output
