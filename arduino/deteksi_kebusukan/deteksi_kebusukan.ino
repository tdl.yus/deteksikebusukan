#include <LiquidCrystal_I2C.h>
#include <TimerOne.h>
#include <math.h>

unsigned long time1, time2, waktu;

//pin LCD Display
LiquidCrystal_I2C lcd(0x27, 20, 4);

//inisalisasi variabel array untuk pengambilan data sensor
float baca[4];

//pin Color Sensor
#define S0  6
#define S1  5
#define S2  4
#define S3  3
#define OUT 2

int   g_count = 0;    // count the frequecy
int   g_array[3];     // store the RGB value
int   g_flag = 0;     // filter of RGB queue
float g_SF[3];        // save the RGB Scale factor

int   R = 0;
int   G = 0;
int   B = 0;

const int gasSensor = 0;
#define Ro 22000
float MQ135_SCALINGFACTOR = 37.58805473; //for NH3
float MQ135_EXPONENT = -3.235365807; //for NH3

// Init TCS3200 and setting Frequency.
void TCS_Init(){
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(OUT, INPUT);

  digitalWrite(S0, LOW);  // OUTPUT FREQUENCY SCALING 2% x10
  digitalWrite(S1, HIGH);
}

// Select the filter color
void TCS_FilterColor(int Level01, int Level02){
  if (Level01 != 0)
    Level01 = HIGH;

  if (Level02 != 0)
    Level02 = HIGH;

  digitalWrite(S2, Level01);
  digitalWrite(S3, Level02);
}

void TCS_Count(){
  g_count ++ ;
}

void TCS_Callback(){
  switch (g_flag){
    case 0:
      TCS_WB(LOW, LOW);              //Filter without Red
      break;
    case 1:
      g_array[0] = g_count * 10;
      TCS_WB(HIGH, HIGH);            //Filter without Green
      break;
    case 2:
      g_array[1] = g_count * 10;
      TCS_WB(LOW, HIGH);             //Filter without Blue
      break;
    case 3:
      g_array[2] = g_count * 10;
      TCS_WB(HIGH, LOW);             //Clear(no filter)
      break;
    default:
      g_count = 0;
      break;
  }
}

void TCS_WB(int Level0, int Level1) {
  g_count = 0;
  g_flag ++;
  TCS_FilterColor(Level0, Level1);
  Timer1.setPeriod(100000);             
}

void setup(){
  pinMode(12, INPUT);
  lcd.init(); 
  lcd.backlight();
  TCS_Init();
  Serial.begin(9600);
  Timer1.initialize();             // nilai default 1s
  Timer1.attachInterrupt(TCS_Callback); //memanggil fungsi TCS_Callback sebagai fungsi interrupt
  attachInterrupt(0, TCS_Count, RISING); //menjalankan fungsi TCS_Count bila pin INT0 mengalami perubahan dari LOW ke HIGH
  delay(500);

  g_SF[0] = 255.0 / 579.973491 ;    //R Scale factor
  g_SF[1] = 255.0 / 598.707950 ;   //G Scale factor
  g_SF[2] = 255.0 / 819.271348 ;   //B Scale factor

  lcd.begin(16, 2);
  delay(500);
}

void loop(){
  int button = digitalRead(12);
  Serial.println(button);
  if (button == LOW) {
    read_sensor(baca);
    testInput();
    delay(500);
  } else {
    read_sensor(baca);
    Serial.print("Hasil = ");
    Serial.println("Press button to classify");
    Serial.print("");
    lcd.setCursor(1, 0);
    lcd.print("press button");
    lcd.setCursor(0, 1);
    lcd.print("to classify");
    delay(500);
    lcd.clear();
  }
   Serial.println("--------------------------");
}
