
float minVal [4] = {
  0.0, 0.0, 0.0, 0.0
};

float maxVal [4] = {
  255.0, 255.0, 255.0, 100.0
};

float bias_Hidden [4] = {
  3.7495019369330795,
 -3.656327540126608,
 -9.984680452117006,
 -11.152360414417029
};

float weight_Hidden [4][4]= {{
    -9.53752943615178,
  3.5824102760236065,
  -1.0525836867996385,
  2.4722760114555387
}, {
   7.196365805742117, -1.0641718207814663, -1.52601546240707, 6.447708677171792
}, {
   30.91914126872395, -8.233612348898147, 1.203229686445866, 8.966403595091826
}, {
   14.745179221824337, 8.684840792953562, -9.407265646062209, 27.76462245804956
}};

float bias_Output [3] = {
    -8.335209788224898, -2.859117305837435, 5.227893654004517
};

float weight_Output [3][4]= {{
  -0.8242659526354096,
  4.379764526620683,
  -0.9353254386235187,
  20.635342274207932
}, {
   -7.305441178679144,
  0.4969536488032304,
  15.488341034347345,
  -18.743997373643523
}, {
  2.9723409314589313,
  -6.3519774898475845,
  -9.881028570934102,
  -12.6057285686095
}};

float activationFunctionBinary(float d){
  return 1.0 / (1.0 + exp(-d));
}

void activation (float y[]){
    float maxVal = -99999999;
    int maxIndex = -1;
    for(int i =0 ; i<3;i++){
      if(y[i]>maxVal){
        maxVal=y[i];
        maxIndex=i;
      }
    }
    String outputString="";
    if(maxIndex == 0){
      outputString = "segar";
    }
    else if(maxIndex == 1){
      outputString = "sedang";
    }
    else if(maxIndex == 2){
      outputString = "busuk";
    }
    else{
      outputString = "unknown";
    }
    lcd.print(outputString);
    Serial.print("Hasil = ");
    Serial.println(outputString);
}

void testInput(){
  float normalized_masukan [4] = {};
  for(int i=0; i<4;i++){
    normalized_masukan[i] = (baca[i]-minVal[i])/(maxVal[i]-minVal[i]);
  };
  float activatedZ [4]={0.0,0.0,0.0,0.0};
  for(int i = 0; i<4; i++){
    float temp_inZ = 0;
    for(int j = 0; j<4; j++){
      Serial.println(normalized_masukan[j]);
      temp_inZ += normalized_masukan[j] * weight_Hidden[i][j];
    }
    temp_inZ += bias_Hidden[i];
    activatedZ[i] = activationFunctionBinary(temp_inZ);
    Serial.println("activated z");
    Serial.println(activatedZ[i]);
  }

  float activatedY [3]={0.0,0.0,0.0};
  for(int i = 0; i<3; i++){
    float temp_inY = 0;
    for(int j = 0; j<4; j++){
      Serial.println(activatedZ[j]);
      temp_inY += activatedZ[j] * weight_Output[i][j];
    }
    temp_inY += bias_Output[i];
    activatedY[i] = activationFunctionBinary(temp_inY);
    Serial.println("activated y");
    Serial.println(activatedY[i]);
  }
  activation(activatedY);
}
