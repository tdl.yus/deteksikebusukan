void read_sensor(float arraybaca[4]){
  //view PPM Amonia
  float analog = analogRead(gasSensor);
  float voltage = (analog/1024)*5;
  float Ra = ((Ro / voltage * 5) - Ro);
  float ppm = MQ135_SCALINGFACTOR * pow((Ra / Ro), MQ135_EXPONENT);
  arraybaca[3] = ppm;
  
  arraybaca[3] = 7.60;
  Serial.print("PPM : ");
  Serial.println(arraybaca[3]);
  Serial.print("Volt : ");
  Serial.println(voltage);
  lcd.setCursor(0, 0);
  lcd.print("PPM  : ");
  lcd.print(arraybaca[3]);
  lcd.setCursor(0, 1);
  lcd.print("Volt : ");
  lcd.print(voltage);
  
  delay(1000);
  lcd.clear();

  g_flag = 0;
  R = g_array[0] * g_SF[0];
  G = g_array[1] * g_SF[1];
  B = g_array[2] * g_SF[2];

  if (R > 255) {
    R = 255;
  }
  if (G > 255) {
    G = 255;
  }
  if (B > 255) {
    B = 255;
  }
//  arraybaca[0] = R;
//  arraybaca[1] = G;
//  arraybaca[2] = B;
  arraybaca[0] = 61;
  arraybaca[1] = 32;
  arraybaca[2] = 37;
  Serial.print("R : ");
  Serial.print(round(arraybaca[0]));
  Serial.print("  ");
  Serial.print("G : ");
  Serial.print(round(arraybaca[1]));
  Serial.print("  ");
  Serial.print("B : ");
  Serial.println(round(arraybaca[2]));
  Serial.println(" ");
  lcd.setCursor(1, 0);
  lcd.print("R = ");
  lcd.setCursor(0, 1);
  lcd.print(round(arraybaca[0]));
  lcd.setCursor(7, 0);
  lcd.print("G = ");
  lcd.setCursor(6, 1);
  lcd.print(round(arraybaca[1]));
  lcd.setCursor(13, 0);
  lcd.print("B = ");
  lcd.setCursor(12, 1);
  lcd.print(round(arraybaca[2]));
  delay(500);
  lcd.clear();
}
